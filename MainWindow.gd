extends NinePatchRect

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var content_node

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	
	content_node = get_child(0)
	
	var run_scene = load("res://run.tscn")
	var run_node = run_scene.instance()
	content_node.add_child(run_node)
	
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
