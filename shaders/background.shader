shader_type canvas_item;
render_mode unshaded;

// 1D random numbers
float rand(float n)
{
	return fract(sin(n) * 43758.5453123);
}

// 2D random numbers
vec2 rand2(in vec2 p)
{
	return fract(vec2(sin(p.x * 591.32 + p.y * 154.077), cos(p.x * 391.32 + p.y * 49.077)));
}

// 1D noise
float noise1(float p)
{
	float fl = floor(p);
	float fc = fract(p);
	return mix(rand(fl), rand(fl + 1.0), fc);
}

// voronoi distance noise, based on iq's articles
float voronoi(in vec2 x)
{
	vec2 p = floor(x);
	vec2 f = fract(x);

	vec2 res = vec2(8.0, 8.0);
	for(int j = -1; j <= 1; j ++)
	{
		for(int i = -1; i <= 1; i ++)
		{
			vec2 b = vec2(float(i), float(j));
			vec2 r = vec2(b) - f + rand2(p + b);

			// mannhatan distance
			float d = abs(r.x) + abs(r.y);

			if (d < res.x)
			{
				res.y = res.x;
				res.x = d;
			}
			else if (d < res.y)
			{
				res.y = d;
			}
		}
	}
	return res.y - res.x;
}

void fragment() {
	vec2 resolution = vec2(1920, 1080);
    vec2 texcoord = FRAGCOORD.xy / resolution;
	float time = TIME * 0.5;
	
	vec2 uv = texcoord;
	uv = (uv - 0.5) * 2.0;
	vec2 suv = uv;
	uv *= resolution.x / resolution.y;

	//uv += _Time.y * 0.04;
	uv.x += 50.0;
	uv.y += 278.0;

	// add some noise octaves
	float a = 0.6, f = 2.0;
	float v = 0.0;
	for(int i = 0; i < 4; i ++) // 4 octaves also look nice, its getting a bit slow though
	{	
		float v1 = voronoi(uv * f + 5.0);
		float v2 = 0.0;

		// make the moving electrons-effect for higher octaves
		if(i > 0)
		{
			// of course everything based on voronoi
			v2 = voronoi(uv * f * 0.5 + time);

			float va = 0.0, vb = 0.0;
			va = 1.0 - smoothstep(0.0, 0.1, v1);
			vb = 1.0 - smoothstep(0.0, 0.08, v2);
			v += a * pow(va * (0.5 + vb), 2.0);
		}

		// make sharp edges
		v1 = 1.0 - smoothstep(0.0, 0.1, v1);

		// noise is used as intensity map
		v += a * (noise1(v1 * 5.5 + 0.1));

		f *= 3.0;
		a *= 0.7;
	}

	// slight vignetting
	v *= exp(-0.6 * length(suv)) * 0.9;

	// use tex2D channel0 for color? why not.
	//float3 cexp = tex2D(_MainTex, uv * 0.001).xyz * 3.0 + tex2D(_MainTex, uv * 0.01).xyz;//float3(1.0, 2.0, 4.0);
	//cexp *= 1.4;

	// old blueish color set
	vec3 cexp = vec3(6.0, 4.0, 4.0);

	vec3 col = vec3(pow(v, cexp.x), pow(v, cexp.y), pow(v, cexp.z)) * 2.0;
	
    COLOR = vec4(col,1);
}