enum GridNodeType {
	Empty,
	Basic
	Ingress,
	Egress
}

enum GridLinkDirections {
	None = 0,
	Left = 1,
	Right = 2,
	Up = 4,
	Down = 8,
	All = Left | Right | Up | Down
}

var type
var link_directions

func _init(grid_node_type, grid_node_link_directions):
	type = grid_node_type
	link_directions = grid_node_link_directions
	
func get_type():
	return type

func clear_link_directions():
	link_directions = GridLinkDirections.None
	
func get_link_directions():
	return link_directions
	
func add_link_direction(grid_node_link_direction):
	link_directions |= grid_node_link_direction