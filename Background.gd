extends ColorRect

func _ready():
	
	var screen_rect = get_viewport().get_visible_rect()	
	var titlebar_node = get_node("Titlebar")
	titlebar_node.set_size(Vector2(screen_rect.size.x, 16.0))
	
	var desktop_container_node = get_node("DesktopContainer")
	desktop_container_node.set_size(screen_rect.size)

	set_size(screen_rect.size)

	pass