extends Node2D

var grid_tile_map_node
var grid_node_script
var grid

func _ready():
	set_process_input(true)
	grid_tile_map_node = get_node("GridTileMap")
	grid_node_script = load("res://GridNode.gd")
	grid = create_test_grid()
	load_grid(grid)
	pass
	
func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass
	
func _input(event):
	# Mouse in viewport coordinates
	if event is InputEventMouseButton:
		print("Mouse Click/Unclick at: ", event.position)
		var tile_coordinates = grid_tile_map_node.world_to_map(event.position)
		print("Tile coordinates: ", tile_coordinates)
		var grid_coordinates = to_grid_coordinates(tile_coordinates)
		print("Grid coordinates: ", grid_coordinates)
		
		if grid_coordinates.x < 10 && grid_coordinates.y < 10:
			var grid_node = grid[grid_coordinates.x][grid_coordinates.y]
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Left > 0:
				print("> left");
			
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Right > 0:
				print("> right");
				
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Up > 0:
				print("> up");
				
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Down > 0:
				print("> down");
		
		
func to_grid_coordinates(var tile_coordinates):
	return Vector2(int(tile_coordinates.x / 2), int(tile_coordinates.y / 2))
	
func load_grid(var grid_data):
	grid_tile_map_node.clear()
	var width = grid_data.size()
	var height = grid_data[0].size()
	for x in range(width):
		for y in range(height):
			var grid_node = grid_data[x][y]
			if grid_node.get_type() != grid_node_script.GridNodeType.Empty:
				grid_tile_map_node.set_cell(x * 2, y * 2, 0)
				
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Left > 0:
				grid_tile_map_node.set_cell(x * 2 - 1, y * 2, 2 )
			
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Right > 0:
				grid_tile_map_node.set_cell(x * 2 + 1, y * 2, 2 )
				
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Up > 0:
				grid_tile_map_node.set_cell(x * 2, y * 2 - 1, 1 )
				
			if grid_node.get_link_directions() & grid_node_script.GridLinkDirections.Down > 0:
				grid_tile_map_node.set_cell(x * 2, y * 2 + 1, 1 )
	pass
	
func create_test_grid():
	var width = 10
	var height = 10
	var grid_data = []
	for x in range(width):
		grid_data.append([])
		for y in range(height):
			var grid_node = grid_node_script.new(grid_node_script.GridNodeType.Basic, grid_node_script.GridLinkDirections.None)
			grid_data[x].append(grid_node)
	
	grid_data[0][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[0][1].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[0][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[0][2].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[0][2].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[0][3].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[0][4].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[0][4].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[0][5].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[0][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[0][5].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[0][6].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[0][6].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[0][7].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[0][8].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[0][8].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[0][9].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][1].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][2].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[1][2].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[1][2].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][3].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][3].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[1][4].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[1][4].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[1][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][5].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[1][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[1][6].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][6].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[1][6].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[1][7].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][8].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[1][8].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[1][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[1][9].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[1][9].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[2][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[2][1].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[2][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[2][2].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[2][2].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[2][2].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[2][3].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[2][4].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[2][4].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[2][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[2][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[2][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[2][5].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[2][6].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[2][6].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[2][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[2][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[2][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[2][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[2][9].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[2][9].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[3][0].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][1].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[3][1].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[3][2].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[3][2].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[3][2].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[3][3].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[3][3].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][3].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[3][4].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[3][4].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[3][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[3][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][5].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[3][6].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[3][6].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[3][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[3][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[3][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[3][9].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][0].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][0].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[4][1].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][2].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[4][3].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][3].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[4][3].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[4][4].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[4][6].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[4][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[4][8].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[4][9].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[5][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[5][0].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[5][1].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[5][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[5][2].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[5][3].add_link_direction(grid_node_script.GridLinkDirections.All)
	grid_data[5][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[5][5].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[5][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[5][6].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[5][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[5][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[5][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[5][8].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[5][8].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[5][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[5][9].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[5][9].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[6][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[6][1].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[6][1].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[6][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[6][2].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[6][3].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[6][3].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[6][3].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[6][4].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[6][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[6][5].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[6][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[6][6].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[6][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[6][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[6][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[6][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[6][9].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][0].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][1].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[7][1].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][2].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][3].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[7][4].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[7][4].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[7][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[7][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[7][5].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][5].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[7][6].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[7][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[7][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[7][8].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[7][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[7][9].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[7][9].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][0].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[8][0].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[8][1].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][1].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[8][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][2].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][2].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[8][2].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][3].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][3].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[8][3].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][4].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][4].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[8][4].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][5].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][5].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[8][5].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][6].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[8][6].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[8][6].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[8][7].add_link_direction(grid_node_script.GridLinkDirections.Right)
	grid_data[8][8].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[8][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[8][9].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][0].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][0].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[9][1].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][2].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][3].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[9][3].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][4].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][4].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[9][5].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][5].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][6].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][6].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[9][6].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][7].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][7].add_link_direction(grid_node_script.GridLinkDirections.Left)
	grid_data[9][7].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][8].add_link_direction(grid_node_script.GridLinkDirections.Down)
	grid_data[9][8].add_link_direction(grid_node_script.GridLinkDirections.Up)
	grid_data[9][9].add_link_direction(grid_node_script.GridLinkDirections.Up)
	
	return grid_data
	pass
